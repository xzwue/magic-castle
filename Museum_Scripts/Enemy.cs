﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{

    NavMeshAgent m_navMeshAgent;

    float m_speed = 2.5f;

    int m_life = 10;

    Player m_player;

    float m_rot_speed = 5.0f;

    Animator animator;

    float m_Timer = 2.0f;

    EnemySpawn enemySpawn;

    // Start is called before the first frame update
    void Start()
    {
        m_navMeshAgent = this.GetComponent<NavMeshAgent>();
        m_navMeshAgent.speed = m_speed;

        animator = this.GetComponent<Animator>();

        m_player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    void RotateTo()
    {
        Vector3 dirTo = m_player.transform.position - this.transform.position;
        Vector3 newDir = Vector3.RotateTowards(transform.forward, dirTo, m_rot_speed * Time.deltaTime, 0.0f);
        this.transform.rotation = Quaternion.LookRotation(newDir);
    }

    // Update is called once per frame
    void Update()
    {

        //if (m_player.m_life <= 0)
        //{
        //    return;
        //}
        //m_Timer -= Time.deltaTime;

        //AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);


        //if (stateInfo.fullPathHash == Animator.StringToHash("Base Layer.idle") && !animator.IsInTransition(0))
        //{

        //    animator.SetBool("idle", false);

        //    if (m_Timer > 0)
        //    {
        //        return;
        //    }

        //    if (Vector3.Distance(this.transform.position, m_player.transform.position) <= 1.5f)
        //    {
        //        m_navMeshAgent.ResetPath();
        //        animator.SetBool("attack", true);
        //    }
        //    else
        //    {
        //        m_Timer = 1;

        //        m_navMeshAgent.SetDestination(m_player.transform.position);

        //        animator.SetBool("run", true);
        //    }
        //}

        //if (stateInfo.fullPathHash == Animator.StringToHash("Base Layer.run") && !animator.IsInTransition(0))
        //{
        //    animator.SetBool("run", false);

        //    if (m_Timer <= 0)
        //    {
        //        m_Timer = 1;

        //        m_navMeshAgent.SetDestination(m_player.transform.position);
        //    }

        //    if (Vector3.Distance(this.transform.position, m_player.transform.position) <= 1.5f)
        //    {
        //        m_navMeshAgent.ResetPath();
        //        animator.SetBool("attack", true);
        //    }
        //}


        //if (stateInfo.fullPathHash == Animator.StringToHash("Base Layer.attack") && !animator.IsInTransition(0))
        //{
        //    RotateTo();

        //    animator.SetBool("attack", false);

        //    if (stateInfo.normalizedTime >= 1.0f && (!animator.GetBool("idle") && !animator.GetBool("run")))
        //    {
        //        animator.SetBool("idle", true);

        //        m_Timer = 2;

        //        m_player.OnDamage(1);
        //    }
        //}

        //if (stateInfo.fullPathHash == Animator.StringToHash("Bash Layer.death") && !animator.IsInTransition(0))
        //{

        //    animator.SetBool("death", false);
        //    if (stateInfo.normalizedTime >= 1.0f)
        //    {
        //        GameManager.gm.SetScore(100);

        //        enemySpawn.m_enemyCount--;


        //        Destroy(this.gameObject);
        //    }
        //}


        //如果主角生命为0，什么也不做
        if (m_player.m_life <= 0)
            return;
        //更新计时器
        m_Timer -= Time.deltaTime;

        //获取当前动画状态
        AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);

        //如果处于待机且不是过渡状态
        if (stateInfo.fullPathHash == Animator.StringToHash("Base Layer.idle") && !animator.IsInTransition(0))
        {
            animator.SetBool("idle", false);

            //待机一定时间

            if (m_Timer > 0)
                return;

            //如果距离主角小于1.5米，进入攻击动画状态
            if (Vector3.Distance(this.transform.position, m_player.transform.position) <= 1.5f)
            {
                //停止寻路
                m_navMeshAgent.ResetPath();
                animator.SetBool("attack", true);
            }
            else
            {
                //重置定时器
                m_Timer = 1;

                //设置寻路目标点
                m_navMeshAgent.SetDestination(m_player.transform.position);

                //进入跑步动画状态
                animator.SetBool("run", true);
            }
        }

        //如果处于跑步且不是过渡状态
        if (stateInfo.fullPathHash == Animator.StringToHash("Base Layer.run") && !animator.IsInTransition(0))
        {
            animator.SetBool("run", false);

            //每隔1秒重新定位主角的位置
            if (m_Timer < 0)
            {
                m_navMeshAgent.SetDestination(m_player.transform.position);

                m_Timer = 1;
            }

            //如果距离主角小于1.5米，向主角攻击
            if (Vector3.Distance(this.transform.position, m_player.transform.position) <= 1.5f)
            {
                //停止寻路
                m_navMeshAgent.ResetPath();
                //进入攻击状态
                animator.SetBool("attack", true);
            }
        }

        //如果处于攻击且不是过渡状态
        if (stateInfo.fullPathHash == Animator.StringToHash("Base Layer.attack") && !animator.IsInTransition(0))
        {
            //面向主角
            RotateTo();
            animator.SetBool("attack", false);

            //如果动画播完，重新进入待机状态
            if (stateInfo.normalizedTime >= 1.0f && (!animator.GetBool("idle") && !animator.GetBool("run")))
            {
                animator.SetBool("idle", true);

                //重置计时器待机2秒
                m_Timer = 2;
                m_player.OnDamage(1); //攻击
            }
        }

        //如果处于死亡且不是过渡状态
        if (stateInfo.fullPathHash == Animator.StringToHash("Base Layer.death") && !animator.IsInTransition(0))
        {
            animator.SetBool("death", false);
            //当播放完成死亡动画
            if (stateInfo.normalizedTime >= 1.0f)
            {
                //更新敌人计数
                enemySpawn.m_enemyCount--;

                //加分
                GameManager.gm.SetScore(100);

                //销毁自身
                Destroy(this.gameObject);
            }
        }
    }
}

    public void onDamage(int hit)
    {
        m_life -= hit;

        if (m_life <= 0)
        {
            animator.SetBool("death", true);

            m_navMeshAgent.ResetPath();
        }
    }

    public void init(EnemySpawn enemySpawn)
    {
        this.enemySpawn = enemySpawn;

        this.enemySpawn.m_enemyCount++;
    }
}

