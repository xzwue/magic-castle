﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    private static GameManager gm;

    public Swipe swipe = null;

    public static GameManager getGM
    {
        get
        {
            return gm;
        }
    }

    void Awake()
    {
        gm = this;
    }
}
