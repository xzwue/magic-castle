﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class Loading : MonoBehaviour {

    public Image loading;

    private bool show = false;

    public event Action OnDone;

    //唤醒

    void OnEnable ()
    {
        loading.fillAmount = 0;
    }

    public void Show(bool isShow)
    {
        show = isShow;
    }

    // Update is called once per frame
    void Update () {
        if (show)
        {
            loading.fillAmount += Time.deltaTime * (0.33333f);
            if (loading.fillAmount >= 1)
            {
                if (OnDone != null)
                {
                    OnDone();
                    show = false;
                }
            }
        }
        else
        {
            loading.fillAmount = 0;
        }
    }
}
