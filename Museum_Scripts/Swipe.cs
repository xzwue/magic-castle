﻿using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;

public class Swipe : VRInteractiveItem
{

    public GameObject boli;
    public Transform dongxi;
    public Loading loading;

    Vector3 startPos;
    Quaternion startRotate;
    Vector3 startScale;

    bool doubleClick = false;
    bool isSelected = false;
    bool canMove = false;
    bool isPressLeft = false;

    float moveSpeed = 1f;
    float t;
    float xSpeed = 15.0f;

    Vector2 oldPosition;
    Vector2 newPosition;

    void OnEnable()
    {
        OnOver += HandleOver;
        OnOut += HandleOut;
        boli.GetComponent<Animator>().SetBool("start", true);
        startPos = dongxi.transform.position;
        startRotate = dongxi.transform.rotation;
        startScale = dongxi.transform.localScale;
    }

    void on_DoubleClick()
    {
        if (!doubleClick && isSelected)
        {
            doubleClick = true;
            float oldScale = dongxi.transform.localScale.x;
            float newScale = oldScale * 1.5f;
            dongxi.transform.localScale = new Vector3(newScale, newScale, newScale);
        }
        else if (doubleClick && isSelected)
        {
            doubleClick = false;
            float oldScale = dongxi.transform.localScale.x;
            float newScale = oldScale / 1.5f;
            dongxi.transform.localScale = new Vector3(newScale, newScale, newScale);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!isSelected)
            return;

        if (dongxi.transform.position == Camera.main.transform.position + Vector3.left * 2)
        {
            canMove = false;
        }

        if (canMove)
        {
            dongxi.transform.position = Vector3.MoveTowards(dongxi.transform.position, Camera.main.transform.position + Vector3.left * 2, Time.deltaTime * moveSpeed);
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                float t1 = Time.realtimeSinceStartup;
                if (t1 - t <= 0.3f)
                {
                    on_DoubleClick();
                }
                t = t1;
            }

            if (Input.GetMouseButton(0))
            {
                if (isPressLeft == false)
                {
                    isPressLeft = true;
                    oldPosition = Input.mousePosition;
                    return;
                }
                else
                {
                    newPosition = Input.mousePosition;
                }
                dongxi.transform.Rotate(Vector3.down * (newPosition.x - oldPosition.x) * Time.deltaTime * xSpeed, Space.Self);
                dongxi.transform.Rotate(Vector3.forward * (newPosition.y - oldPosition.y) * Time.deltaTime * xSpeed, Space.Self);
                oldPosition = newPosition;
            }

            if (Input.GetMouseButtonUp(0))
            {
                isPressLeft = false;
            }
        }
    }

    private void HandleOver()
    {
        if (!isSelected && CameraMove.LoadingT)
        {
            loading.Show(true);
            loading.OnDone += HandleDone;
        }
    }

    private void HandleOut()
    {
        if (!isSelected)
        {
            loading.Show(false);
            loading.OnDone -= HandleDone;
        }
    }

    private void HandleDone()
    {
        if (GameManager.getGM.swipe)
        {
            GameManager.getGM.swipe.reset();
        }
        GameManager.getGM.swipe = this;
        isSelected = true;
        canMove = true;
        boli.GetComponent<Animator>().SetBool("start", false);
    }

    public void reset()
    {
        isSelected = false;
        isPressLeft = false;
        doubleClick = false;
        canMove = false;
        boli.GetComponent<Animator>().SetBool("start", true);
        dongxi.transform.position = startPos;
        dongxi.transform.rotation = startRotate;
        dongxi.transform.localScale = startScale;
    }
}
