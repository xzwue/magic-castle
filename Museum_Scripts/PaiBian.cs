﻿using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;
using UnityEngine.UI;

public class PaiBian : VRInteractiveItem
{

    bool showText = false;

    public Text text2;
    public CameraMove cm;
    public Loading loading;

    string pBText = "中国博物馆是中国文物和标本的主要收藏机构、宣传教育机构和科学研究机构，是社会主义科学化事业的重要组成部分。";

    float waitTime = 3.0f;
    float TextNum = 0;

    void OnEnable ()
    {
        showText = false;
        OnOver += HandleOver;
        OnOut += HandleOut;
    }
	// Use this for initialization
	void Start () {
        text2.enabled = false;
        this.GetComponent<Animator>().SetBool("start", true);
	}
	
	// Update is called once per frame
	void Update () {
        if (showText)
        {
            if (text2.text.Length == pBText.Length)
            {
                waitTime -= Time.deltaTime;
                if (waitTime <= 0)
                {
                    showText = false;
                    text2.enabled = false;
                    cm.MoveToThree();
                }
            }
            else
            {
                TextNum += Time.deltaTime * 4;
                text2.text = pBText.Substring(0, (int)TextNum + 1);
            }
        }
        else
        {
            TextNum = 0;
        }
    }

    private void HandleOver ()
    {
        loading.Show(true);
        loading.OnDone += HandleDone;
    }

    private void HandleOut ()
    {
        loading.Show(false);
        loading.OnDone -= HandleDone;
    }

    private void HandleDone ()
    {
        showText = true;
        text2.enabled = true;
        this.GetComponent<Animator>().SetBool("start", false);
    }
}
