﻿using UnityEngine;
using System.Collections;

public class CameraMove : MonoBehaviour
{

    Vector3 paiBian;
    Vector3 threeRoom;
    Vector3 twoRoom;

    bool startMove = true;
    bool moveToThree = false;
    bool moveToTwo = false;
    public static bool LoadingT = false;

    float waitTime = 3.0f;
    float speed = 0.5f;

    void OnEnable()
    {
        paiBian = new Vector3(0.5f, -0.7f, -11f);
        threeRoom = new Vector3(-3.4f, 0.02f, 8f);
        twoRoom = new Vector3(0.8f, -1f, -2.4f);
    }

    //Update is called once per frame
    void Update()
    {
        if (this.transform.position == paiBian)
        {
            startMove = false;
        }

        if (this.transform.position == threeRoom)
        {
            moveToThree = false;
            waitTime -= Time.deltaTime;
            if (waitTime <= 0)
            {
                speed = 1f;
                moveToTwo = true;
            }
        }

        if (this.transform.position == twoRoom)
        {
            LoadingT = true;
            moveToTwo = false;
        }

        if (startMove)
        {
            this.transform.position = Vector3.MoveTowards(this.transform.position, paiBian, Time.deltaTime * speed);
        }

        if (moveToThree)
        {
            this.transform.position = Vector3.MoveTowards(this.transform.position, threeRoom, Time.deltaTime * speed);

        }


        if (moveToTwo)
        {
            this.transform.position = Vector3.MoveTowards(this.transform.position, twoRoom, Time.deltaTime * speed);
        }
    }

    public void MoveToThree()
    {
        moveToThree = true;
        speed = 1.5f;
    }
}
